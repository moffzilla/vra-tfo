output "mysql_ip_address" {
  // Works for simple deployments with only one machine resource with count as 1.
  description = "IP address property of first resource in a vRA deployment"
  value       = jsondecode(sort(vra_deployment.this.resources.*.properties_json)[0]).address
}

