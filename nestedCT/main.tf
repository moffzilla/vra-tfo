terraform {
  required_providers {
    vra = {
      source  = "vmware/vra"
      version = "0.3.2"
    }
  }
}

provider vra {
  url           = var.vra_url
  refresh_token = var.vra_refresh_token
  insecure      = true
}

resource "vra_deployment" "this" {
  name        = var.deployment_name
  description = var.deployment_description

  blueprint_id      = var.blueprint_id
  blueprint_version = var.blueprint_version
  project_id        = var.project_id

  inputs = {
    image = var.image
    size = var.size
    username = var.username
    password = var.password
    workloadType = var.workloadtype
  }

  timeouts {
    create = "30m"
    delete = "30m"
    update = "30m"
  }
}



