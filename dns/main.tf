terraform {
  required_providers {
    dns = {
      source = "hashicorp/dns"
    }
  }
}

provider "dns" {
  update {
    server = var.server
  }
}

resource "dns_a_record_set" "www" {
  zone = "corp.local."
  name = var.record_name
  addresses = [
    var.record_addr 
  ]
  ttl = 300
}
